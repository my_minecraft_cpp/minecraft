#define _USE_MATH_DEFINES
#include <minecraft/entity.hpp>

#include <stdlib.h>
#include <cmath>
#include <minecraft/cpp_utils/random/random.hpp>
#include <minecraft/cpp_utils/math/math.hpp>


using minecraft::cpp_utils::math::equal_float;

namespace minecraft
{

void Entity::setPos(float x, float y, float z)
{
	this->x = x;
	this->y = y;
	this->z = z;

	float w = 0.3F; // Размеры половины игрока по ширине и длине (2w - полный размер игрока)
	float h = 0.9F; // Размеры половины игрока по высоте (2h - полный рост игрока)

	bb = phys::AABB(x - w, y - h, z - w, x + w, y + h, z + w);
}

void Entity::resetPos()
{
	float x = cpp_utils::random::uniform_random() * static_cast<float>(level->width);
	float y = static_cast<float>(level->depth + 10);
	float z = cpp_utils::random::uniform_random() * static_cast<float>(level->height);

	setPos(x, y, z);
}

void Entity::turn(float xo, float yo)
{
	yRot = static_cast<float>(yRot + xo * 0.15F);
	xRot = static_cast<float>(xRot - yo * 0.15F);

	if (xRot < -90) xRot = -90;
	if (xRot > 90) xRot = 90;
}

void Entity::tick()
{
	xo = x;
	yo = y;
	zo = z;
}

void Entity::move(float xa, float ya, float za)
{
	// Сохранение текущих показателей перемещения
	float xaOrg = xa;
	float yaOrg = ya;
	float zaOrg = za;

	// Получение набора регионов, соответствующих блокам, которые мог задеть игрок при перемещении
	std::vector<phys::AABB> aABBs = level->getCubes(bb.expand(xa, ya, za));

	// Проверка столкновения с каждым блоком по оси y
	for (unsigned int i = 0; i < aABBs.size(); i++)
		ya = aABBs[i].clipYCollide(bb, ya);
	bb.move(0, ya, 0);

	// Проверка столкновения с каждым блоком по оси x
	for (unsigned int i = 0; i < aABBs.size(); i++)
		xa = aABBs[i].clipXCollide(bb, xa);
	bb.move(xa, 0, 0);

	// Проверка столкновения с каждым блоком по оси z
	for (unsigned int i = 0; i < aABBs.size(); i++)
		za = aABBs[i].clipZCollide(bb, za);
	bb.move(0, 0, za);

	// Проверка на наличие земли под ногами
	onGround = !equal_float(yaOrg, ya) && (yaOrg < 0);

	// Если есть столкновение (сохраненные показания отличаются от текущих), то обнулить ускорение
	if (!equal_float(xaOrg, xa)) xd = 0;
	if (!equal_float(yaOrg, ya)) yd = 0;
	if (!equal_float(zaOrg, za)) zd = 0;

	x = (bb.x0 + bb.x1) / 2;
	y = bb.y0 + heightOffset;
	z = (bb.z0 + bb.z1) / 2;
}

void Entity::moveRelative(float xa, float za, float speed)
{
	const float DEG_TO_RAD = static_cast<float>(M_PI) / 180.0F;
	float dist = xa * xa + za * za;

	if (dist >= 0.01F) {
		dist = speed / sqrtf(dist);

		xa *= dist;
		za *= dist;

		float sinR = sinf(yRot * DEG_TO_RAD);
		float cosR = cosf(yRot * DEG_TO_RAD);

		xd += xa * cosR - za * sinR;
		zd += za * cosR + xa * sinR;
	}
}

}  // namespace minecraft