#include <minecraft/character/vec3.hpp>


namespace minecraft::character
{

Vec3 Vec3::interpolateTo(const Vec3& t, float p) const
{
	float xt = x + (t.x - x) * p;
	float yt = y + (t.y - y) * p;
	float zt = z + (t.z - z) * p;
	return Vec3(xt, yt, zt);
}

}  // namespace minecraft::character