#define _USE_MATH_DEFINES
#include <minecraft/character/zombie.hpp>

#include <cmath>
#include <stdlib.h>


#include <minecraft/cpp_utils/time/time.hpp>
#include <minecraft/cpp_utils/random/random.hpp>

#define sinF(value) \
	(static_cast<float>(sin(value)))


using minecraft::cpp_utils::random::uniform_random;

namespace minecraft::character
{

Zombie::Zombie(level::Level& level, float x, float y, float z) : Entity(level)
{
	rotA = (uniform_random() + 1) * 0.01F;

	this->x = x;
	this->y = y;
	this->z = z;

	timeOffs = uniform_random() * 1239813.0F;
	rot = uniform_random() * static_cast<float>(M_PI) * 2.0F;
	speed = 1;

	head = Cube(0, 0);
	head.addBox(-4, -8, -4, 8, 8, 8);

	body = Cube(16, 16);
	body.addBox(-4, 0, -2, 8, 12, 4);

	arm0 = Cube(40, 16);
	arm0.addBox(-3, -2, -2, 4, 12, 4);
	arm0.setPos(-5, 2, 0);

	arm1 = Cube(40, 16);
	arm1.addBox(-1, -2, -2, 4, 12, 4);
	arm1.setPos(5, 2, 0);

	leg0 = Cube(0, 16);
	leg0.addBox(-2, 0, -2, 4, 12, 4);
	leg0.setPos(-2, 12, 0);

	leg1 = Cube(0, 16);
	leg1.addBox(-2, 0, -2, 4, 12, 4);
	leg1.setPos(2, 12, 0);
}

void Zombie::tick()
{
	xo = x;
	yo = y;
	zo = z;

	rot += rotA;
	rotA *= 0.99F;
	rotA = rotA + (uniform_random() - uniform_random()) * uniform_random() * uniform_random() * 0.01F;

	float xa = sinf(rot);
	float ya = cosf(rot);

	if (onGround && uniform_random() < 0.01F) yd = 0.12F; // Добавить ускарение по оси y (прыжек вверх)

	moveRelative(xa, ya, onGround ? 0.02F : 0.005F);
	
	yd = yd - 0.005F; // Эмитация земного притяжения
	move(xd, yd, zd);

	// Торможение ускорения
	xd *= 0.91F;
	yd *= 0.98F;
	zd *= 0.91F;

	if (y < 0) resetPos();

	// Более медленное ускорение перемещения в воздухе
	if (onGround)
	{
		xd *= 0.8F;
		zd *= 0.8F;
	}
}

void Zombie::render(float a)
{
	const float DEG_TO_RAD = static_cast<float>(M_PI) / 180.0F;

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, Textures::loadTexture("objects/char.png", GL_NEAREST));
	glPushMatrix();

	double time = static_cast<double>(cpp_utils::time::getTimeMicrosecond()) / 1E5 * static_cast<double>(speed) + timeOffs;
	float size = 21.0F / 360.0F;
	float yy = -abs(static_cast<float>(sin(time * 0.6662))) * 5 - 23;
	
	glTranslatef(xo + (x - xo) * a, yo + (y - yo) * a, zo + (z - zo) * a);
	glScalef(1, -1, 1);
	glScalef(size, size, size);
	glTranslatef(0, yy, 0);
	glRotatef(rot * DEG_TO_RAD + 180.0F, 0, 1, 0);

	// Случайно двигать всеми частями тела
	head.yRot = sinF(time * 0.83) * 1.0F;
	head.xRot = sinF(time) * 0.8F;

	arm0.xRot = sinF(time * 0.6662 + M_PI) * 2.0F;
	arm0.zRot = (sinF(time * 0.2312) + 1) * 1.0F;

	arm1.xRot = sinF(time * 0.6662) * 2.0F;
	arm1.zRot = (sinF(time * 0.2812) - 1.0F) * 1.0F;

	leg0.xRot = sinF(time * 0.6662) * 1.4F;
	leg1.xRot = sinF(time * 0.6662 + M_PI) * 1.4F;

	head.render();
	body.render();
	arm0.render();
	arm1.render();
	leg0.render();
	leg1.render();

	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
}

}  // namespace minecraft::character