#include <minecraft/character/polygon.hpp>


namespace minecraft::character
{

void Polygon::render() const
{
	glColor3f(1.0f, 1.0f, 1.0f);

	for (int32_t i = 3; i >= 0; i--)
	{
		const Vertex& v = vertices[static_cast<uint32_t>(i)];
		glTexCoord2f(v.u / 64.0f, v.v / 32.0f);
		glVertex3f(v.pos.x, v.pos.y, v.pos.z);
	}
}

}  // namespace minecraft::character