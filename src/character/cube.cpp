#include <minecraft/character/cube.hpp>


namespace minecraft::character
{

void Cube::addBox(const float x0, const float y0, const float z0, const int w, const int h, const int d)
{
	float x1 = x0 + static_cast<float>(w), y1 = y0 + static_cast<float>(h), z1 = z0 + static_cast<float>(d);
	Vertex u0(x0, y0, z0, 0, 0), u1(x1, y0, z0, 0, 8), u2(x1, y1, z0, 8, 8), u3(x0, y1, z0, 8, 0);
	Vertex l0(x0, y0, z1, 0, 0), l1(x1, y0, z1, 0, 8), l2(x1, y1, z1, 8, 8), l3(x0, y1, z1, 8, 0);

	vertices = { u0, u1, u2, u3, l0, l1, l2, l3 };
	polygons = {
		Polygon({ l1, u1, u2, l2 }, xTexOffs + d + w, yTexOffs + d, xTexOffs + d + w + d, yTexOffs + d + h),
		Polygon({ u0, l0, l3, u3 }, xTexOffs, yTexOffs + d, xTexOffs + d, yTexOffs + d + h),
		Polygon({ l1, l0, u0, u1 }, xTexOffs + d, yTexOffs + 0, xTexOffs + d + w, yTexOffs + d),
		Polygon({ u2, u3, l3, l2 }, xTexOffs + d + w, yTexOffs + 0, xTexOffs + d + w + w, yTexOffs + d),
		Polygon({ u1, u0, u3, u2 }, xTexOffs + d, yTexOffs + d, xTexOffs + d + w, yTexOffs + d + h),
		Polygon({ l0, l1, l2, l3 }, xTexOffs + d + w + d, yTexOffs + d, xTexOffs + d + w + d + w, yTexOffs + d + h)
	};
}

void Cube::render() const
{
	const float DEG_TO_RAD = 57.29578F;

	glPushMatrix();
	
	glTranslatef(x, y, z);
	glRotatef(zRot * DEG_TO_RAD, 0, 0, 1);
	glRotatef(yRot * DEG_TO_RAD, 0, 1, 0);
	glRotatef(xRot * DEG_TO_RAD, 1, 0, 0);

	glBegin(GL_QUADS);
	for (const Polygon& p : polygons) p.render();
	glEnd();

	glPopMatrix();
}

}  // namespace minecraft::character