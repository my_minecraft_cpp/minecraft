#include <minecraft/phys/AABB.hpp>


namespace minecraft::phys
{

AABB AABB::expand(float xa, float ya, float za) const
{
	return AABB(
		x0 + ((xa < 0) ? xa : 0),
		y0 + ((ya < 0) ? ya : 0),
		z0 + ((za < 0) ? za : 0),
		x1 + ((xa > 0) ? xa : 0),
		y1 + ((ya > 0) ? ya : 0),
		z1 + ((za > 0) ? za : 0)
	);
}

AABB AABB::grow(float xa, float ya, float za) const
{
	return AABB(x0 - xa, y0 - ya, z0 - za, x1 + xa, y1 + ya, z1 + za);
}

float AABB::clipXCollide(const AABB& c, float xa) const
{
	if (c.y1 <= y0 || c.y0 >= y1) return xa;
	if (c.z1 <= z0 || c.z0 >= z1) return xa;

	if (xa > 0 && c.x1 <= x0)
	{
		float max = x0 - c.x1 - epsilon;

		if (max < xa) xa = max;
	}

	if (xa < 0 && c.x0 >= x1) {
		float max = x1 - c.x0 + epsilon;

		if (max > xa) xa = max;
	}

	return xa;
}

float AABB::clipYCollide(const AABB& c, float ya) const
{
	if (c.x1 <= x0 || c.x0 >= x1) return ya;
	if (c.z1 <= z0 || c.z0 >= z1) return ya;

	if (ya > 0 && c.y1 <= y0)
	{
		float max = y0 - c.y1 - epsilon;

		if (max < ya) ya = max;
	}

	if (ya < 0 && c.y0 >= y1)
	{
		float max = y1 - c.y0 + epsilon;

		if (max > ya) ya = max;
	}

	return ya;
}

float AABB::clipZCollide(const AABB& c, float za) const
{
	if (c.x1 <= x0 || c.x0 >= x1) return za;
	if (c.y1 <= y0 || c.y0 >= y1) return za;

	if (za > 0 && c.z1 <= z0)
	{
		float max = z0 - c.z1 - epsilon;

		if (max < za) za = max;
	}

	if (za < 0.0F && c.z0 >= z1)
	{
		float max = z1 - c.z0 + epsilon;

		if (max > za) za = max;
	}

	return za;
}

bool AABB::intersects(const AABB& c) const
{
	return (c.x1 > x0) && (c.x0 < x1) &&
		(c.y1 > y0) && (c.y0 < y1) &&
		(c.z1 > z0) && (c.z0 < z1);
}

void AABB::move(float xa, float ya, float za)
{
	x0 += xa; x1 += xa;
	y0 += ya; y1 += ya;
	z0 += za; z1 += za;
}

}  // namespace minecraft::phys