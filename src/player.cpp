#include <minecraft/player.hpp>

#include <minecraft/cpp_utils/input/keyboard.hpp>


using minecraft::cpp_utils::input::Keyboard;


namespace minecraft
{

Player::Player(level::Level& level) : Entity(level)
{
	heightOffset = 1.62F;
}

void Player::tick()
{
	xo = x;
	yo = y;
	zo = z;

	float xa = 0;
	float ya = 0;

	// Работа с клавиатурой
	Keyboard& keyboard = Keyboard::getInstance();
	if (keyboard.isKeyDown(GLFW_KEY_R)) resetPos();
	if (keyboard.isKeyDown(GLFW_KEY_UP) || keyboard.isKeyDown(GLFW_KEY_W)) ya--;
	if (keyboard.isKeyDown(GLFW_KEY_DOWN) || keyboard.isKeyDown(GLFW_KEY_S)) ya++;
	if (keyboard.isKeyDown(GLFW_KEY_LEFT) || keyboard.isKeyDown(GLFW_KEY_A)) xa--;
	if (keyboard.isKeyDown(GLFW_KEY_RIGHT) || keyboard.isKeyDown(GLFW_KEY_D)) xa++;
	if (keyboard.isKeyDown(GLFW_KEY_SPACE) && onGround) yd = 0.12F; // Добавить ускарение по оси y (прыжек вверх)

	moveRelative(xa, ya, onGround ? 0.02F : 0.005F); 

	yd = yd - 0.005F; // Эмитация земного притяжения
	move(xd, yd, zd);

	// Торможение ускорения
	xd *= 0.91F;
	yd *= 0.98F;
	zd *= 0.91F;

	// Более медленное ускорение перемещения в воздухе
	if (onGround)
	{
		xd *= 0.8F;
		zd *= 0.8F;
	}
}

}  // namespace minecraft