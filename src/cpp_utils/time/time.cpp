#include <minecraft/cpp_utils/time/time.hpp>


#ifdef _WIN32
	#include <Windows.h>

	namespace minecraft::cpp_utils::time
	{

	uint64_t getTimeNanosecond()
	{
		static uint64_t freq = 0;

		if (freq == 0)
		{
			LARGE_INTEGER freq_temp;
			QueryPerformanceFrequency(&freq_temp);
			freq = static_cast<uint64_t>((1E12) / freq_temp.QuadPart);
		}

		LARGE_INTEGER t;
		QueryPerformanceCounter(&t);
		return static_cast<uint64_t>(t.QuadPart * freq / 1000);
	}

	uint64_t getTimeMicrosecond()
	{
		FILETIME t;
		GetSystemTimeAsFileTime(&t);

		return (*((uint64_t*)&t)) / 10;
	}

	uint64_t getTimeMillisecond()
	{
		FILETIME t;
		GetSystemTimeAsFileTime(&t);

		return (*((uint64_t*)&t)) / 10000;
	}

	}  // namespace minecraft::cpp_utils::time
#else
	#include <ctime>

	namespace minecraft::cpp_utils::time
	{

	uint64_t getTimeNanosecond()
	{
		timespec time;
		clock_gettime(CLOCK_REALTIME, &time);
		return static_cast<uint64_t>(time.tv_sec) * 1000000000 + static_cast<uint64_t>(time.tv_nsec);
	}

	uint64_t getTimeMicrosecond()
	{
		timespec time;
		clock_gettime(CLOCK_REALTIME, &time);
		return static_cast<uint64_t>(time.tv_sec) * 1000000 + static_cast<uint64_t>(time.tv_nsec) / 1000;
	}

	uint64_t getTimeMillisecond()
	{
		timespec time;
		clock_gettime(CLOCK_REALTIME, &time);
		return static_cast<uint64_t>(time.tv_sec) * 1000 + static_cast<uint64_t>(time.tv_nsec) / 1000000;
	}

	}  // namespace minecraft::cpp_utils::time
#endif