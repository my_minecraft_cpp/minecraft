#include <minecraft/cpp_utils/image/image.hpp>

#include <png.h>
#include <cstring>


namespace minecraft::cpp_utils::image
{

bool Image::is_png_image(const void* data, size_t size)
{
	if (size < 8) return false;
	return png_sig_cmp(static_cast<png_const_bytep>(data), 0, 8) == 0;
}

struct ReadDataHandle
{
	const png_byte* data;
	const png_size_t size;
    png_size_t offset;
};

void read_from_input_stream(png_structp png_ptr, png_bytep raw_data, png_size_t read_length)
{
	ReadDataHandle& handle = *static_cast<ReadDataHandle*>(png_get_io_ptr(png_ptr));
	const png_byte* png_src = handle.data + handle.offset;
	memcpy(raw_data, png_src, read_length);
	handle.offset += read_length;
}

Image Image::load_png_image(const void* data, size_t size)
{
	if (!is_png_image(data, size)) return Image();

	png_structp png_ptr;
	png_infop info_ptr;

	if ((png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL)) == NULL)
	{
		return Image();
	}
	if ((info_ptr = png_create_info_struct(png_ptr)) == NULL)
	{
		png_destroy_read_struct(&png_ptr, NULL, NULL);
		return Image();
	}
	// check if libpng encounters an error
	if (setjmp(png_jmpbuf(png_ptr)) != 0)
	{
		png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
		return Image();
	}

	ReadDataHandle data_handle = ReadDataHandle{ static_cast<const png_byte*>(data), size, 0 };
	png_set_read_fn(png_ptr, &data_handle, read_from_input_stream);
	// tell libpng we already read the signature
    png_set_sig_bytes(png_ptr, 8);
	data_handle.offset = 8;

	png_read_info(png_ptr, info_ptr);

	png_uint_32 width = 0;
	png_uint_32 height = 0;
	png_uint_32 depth = 0;
	int colorType = -1;
	if (png_get_IHDR(png_ptr, info_ptr, &width, &height, reinterpret_cast<int*>(&depth), &colorType, NULL, NULL, NULL) != 1)
	{
		png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
		return Image();
	}

	bool transparency = false;
    if (png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS))
    {
        png_set_tRNS_to_alpha(png_ptr);
        transparency = true;
    }

	// Expands PNG with less than 8bits per channel to 8bits.
    if (depth < 8)
    {
        png_set_packing(png_ptr);
    }
	// Shrinks PNG with 16bits per color channel down to 8bits.
    else if (depth == 16)
    {
        png_set_strip_16(png_ptr);
    }

	if (colorType == PNG_COLOR_TYPE_PALETTE)
	{
		png_set_palette_to_rgb(png_ptr);
	}
	else if (colorType == PNG_COLOR_TYPE_RGBA)
	{
		transparency = true;
	}
	
	png_read_update_info(png_ptr, info_ptr);

	png_uint_32 channels = static_cast<png_uint_32>(png_get_channels(png_ptr, info_ptr));
	depth = png_get_bit_depth(png_ptr, info_ptr);

	png_bytepp row_data = new png_bytep[height];
	std::vector<uint8_t> img_data(width * height * depth * channels / 8);
	const size_t stride = width * depth * channels / 8;

	for (size_t i = 0; i < height; i++)
		row_data[i] = img_data.data() + i * stride;

	png_read_image(png_ptr, row_data);
	delete[] row_data;
	png_destroy_read_struct(&png_ptr, &info_ptr, NULL);

	return Image(img_data, height, width, depth, channels, transparency);
}

}  // namespace minecraft::cpp_utils::image