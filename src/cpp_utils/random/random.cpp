#include <minecraft/cpp_utils/random/random.hpp>

#include <cstdlib>

namespace minecraft::cpp_utils::random
{

float uniform_random()
{
	return static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
}

}  // namespace minecraft::cpp_utils::random