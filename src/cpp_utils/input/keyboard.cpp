#include <minecraft/cpp_utils/input/keyboard.hpp>


namespace minecraft::cpp_utils::input
{

Keyboard Keyboard::instance;

bool Keyboard::isKeyDown(int key) const
{
    int state = glfwGetKey(window, key);
    return state == GLFW_PRESS || state == GLFW_REPEAT;
}

}  // namespace minecraft::cpp_utils::input