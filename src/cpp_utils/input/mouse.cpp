#include <minecraft/cpp_utils/input/mouse.hpp>

#include <functional>


namespace minecraft::cpp_utils::input
{

Mouse Mouse::instance;

void Mouse::mouse_button_callback([[maybe_unused]] GLFWwindow* window, int button, int action, int mods)
{
	Mouse::getInstance().queueButtonEvent.push({button, action, mods});
}

void Mouse::registerButtonCallback()
{
	glfwSetMouseButtonCallback(window, &Mouse::mouse_button_callback);
}

bool Mouse::nextButtonEvent()
{
	if (queueButtonEvent.empty()) return false;
	lastButtonEvent = queueButtonEvent.front();
	queueButtonEvent.pop();
	return true;
}

void Mouse::setInputMode(int mode, int value) const
{
	glfwSetInputMode(window, mode, value);
}

void Mouse::showCursor(bool show) const
{
	if (show)
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	else
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}

void Mouse::setCursorPos(int xpos, int ypos) const
{
	glfwSetCursorPos(window, xpos, ypos);
}

void Mouse::getCursorPos(int* xpos, int* ypos) const
{
    double _xpos, _ypos;
	glfwGetCursorPos(window, &_xpos, &_ypos);
	*xpos = static_cast<int>(_xpos);
	*ypos = static_cast<int>(_ypos);
}

bool Mouse::isButtonDown(int button) const
{
	int state = glfwGetMouseButton(window, button);
	return state == GLFW_PRESS;
}

}  // namespace minecraft::cpp_utils::input