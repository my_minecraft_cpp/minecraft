#include <minecraft/cpp_utils/math/math.hpp>

#ifdef _WIN32
	#include <cstdlib>
#else
	#include <stdlib.h>
#endif // _WIN32


namespace minecraft::cpp_utils::math
{

bool equal_float(float a, float b)
{
	return abs(a - b) < 1e-5;
}

}  // namespace minecraft::cpp_utils::math