#include <minecraft/level/tesselator.hpp>


namespace minecraft::level
{

void Tesselator::vertex(float x, float y, float z)
{
	vertexBuffer[vertices * 3 + 0] = x;
	vertexBuffer[vertices * 3 + 1] = y;
	vertexBuffer[vertices * 3 + 2] = z;

	texCoordBuffer[vertices * 2 + 0] = u;
	texCoordBuffer[vertices * 2 + 1] = v;

	colorBuffer[vertices * 3 + 0] = r;
	colorBuffer[vertices * 3 + 1] = g;
	colorBuffer[vertices * 3 + 2] = b;

	vertices++;

	if (vertices >= MAX_VERTICES) flush();
}

void Tesselator::flush()
{
	if (vertices <= 0) return;

	glVertexPointer(3, GL_FLOAT, 0, vertexBuffer);
	if (hasTexture) glTexCoordPointer(2, GL_FLOAT, 0, texCoordBuffer);
	if (hasColor) glColorPointer(3, GL_FLOAT, 0, colorBuffer);

	glEnableClientState(GL_VERTEX_ARRAY);
	if (hasTexture) glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	if (hasColor) glEnableClientState(GL_COLOR_ARRAY);

	glDrawArrays(GL_QUADS, 0, vertices);

	glDisableClientState(GL_VERTEX_ARRAY);
	if (hasTexture) glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	if (hasColor) glDisableClientState(GL_COLOR_ARRAY);

	clear();
}

}  // namespace minecraft::level