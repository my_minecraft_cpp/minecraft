#include <minecraft/level/chunk.hpp>


namespace minecraft::level
{

Tesselator Chunk::t = Tesselator();
int Chunk::rebuiltThisFrame = 0;
int Chunk::updates = 0;

Chunk::Chunk(Level& level, int x0, int y0, int z0, int x1, int y1, int z1)
	: level(&level), x0(x0), y0(y0), z0(z0), x1(x1), y1(y1), z1(z1)
{
	aabb = phys::AABB(
		static_cast<float>(x0), static_cast<float>(y0), static_cast<float>(z0),
		static_cast<float>(x1), static_cast<float>(y1), static_cast<float>(z1));
	lists = glGenLists(2);  // Получить 2 списка
}

void Chunk::rebuild(uint32_t layer)
{
	if (rebuiltThisFrame == 2) return;

	dirty = false;
	updates++;
	rebuiltThisFrame++;
	
	GLuint id = Textures::loadTexture("objects/terrain.png", GL_NEAREST);

	glNewList(lists + layer, GL_COMPILE);

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, id);

	t.init();

	for (int x = x0; x < x1; x++) 
		for (int y = y0; y < y1; y++) 
			for (int z = z0; z < z1; z++)
				if (level->isTile(x, y, z))
				{
					bool tex = y != static_cast<int>(level->depth * 2 / 3);

					if (tex)
						Tile::grass.render(t, *level, layer, x, y, z);  // Блок травы
					else
						Tile::rock.render(t, *level, layer, x, y, z);  // Блок камня
				}

	t.flush();

	glDisable(GL_TEXTURE_2D);
	glEndList();
}

void Chunk::render(uint32_t layer)
{
	if (dirty)
	{
		rebuild(0);
		rebuild(1);
	}
	
	glCallList(lists + layer);
}

}  // namespace minecraft::level