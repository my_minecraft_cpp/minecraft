#include <minecraft/level/level.hpp>

#include <cstdio>
#include <algorithm>
#include <zlib.h>


namespace minecraft::level
{

Level::Level(uint32_t w, uint32_t h, uint32_t d) : width(w), height(h), depth(d)
{
	blocks = new uint8_t[w * h * d];
	lightDepths = new int[w * h];

	// Создание плоского мира
	for (uint32_t x = 0; x < w; x++) {
		for (uint32_t y = 0; y < d; y++) {
			for (uint32_t z = 0; z < h; z++) {
				uint32_t i = (y * this->height + z) * this->width + x;
				blocks[i] = static_cast<uint8_t>(y <= d * 2 / 3 ? 1 : 0);
			}
		}
	}

	calcLightDepths(0, 0, static_cast<int>(w), static_cast<int>(h));  // Отправка на просчет света/тени
	load(); // Загрузка мира из level.dat
}

Level::~Level()
{
	if (blocks) delete [] blocks;
	levelListeners.clear();
}

void Level::load()
{
	gzFile file = gzopen("level.dat", "r");
	if (file)
	{
		gzread(file, blocks, width * height * depth);
		gzclose(file);

		calcLightDepths(0, 0, static_cast<int>(width), static_cast<int>(height));  // Отправка на просчет света/тени

		for (LevelListener*& l : levelListeners) l->allChanged();
	}
}

void Level::save() const
{
	gzFile file = gzopen("level.dat", "w");
	gzwrite(file, blocks, width * height * depth);
	gzclose(file);
}

void Level::calcLightDepths(int x0, int y0, int x1, int y1)
{
	for (int x = x0; x < x0 + x1; x++) {
		for (int z = y0; z < y0 + y1; z++) {
			int oldDepth = lightDepths[x + z * static_cast<int>(width)];

			int y = static_cast<int>(depth) - 1;
			while (y > 0 && !isLightBlocker(x, y, z)) y--;

			lightDepths[x + z * static_cast<int>(width)] = y;
			if (oldDepth != y) {
				int yl0 = oldDepth < y ? oldDepth : y;
				int yl1 = oldDepth > y ? oldDepth : y;

				for (unsigned int i = 0; i < levelListeners.size(); i++) {
					levelListeners[i]->lightColumnChanged(x, z, yl0, yl1);
				}
			}
		}
	}
}

void Level::addListener(LevelListener* levelListener)
{
	levelListeners.push_back(levelListener);
}

void Level::removeListener(LevelListener* levelListener)
{
	auto it = std::find(levelListeners.begin(), levelListeners.end(), levelListener);
	levelListeners.erase(it);
}

bool Level::isTile(int x, int y, int z) const
{
	if (!(x >= 0 && y >= 0 && z >= 0 && x < static_cast<int>(width) && y < static_cast<int>(depth) && z < static_cast<int>(height))) return false;
	return blocks[(y * static_cast<int>(height) + z) * static_cast<int>(width) + x] == 1;
}

bool Level::isSolidTile(int x, int y, int z) const
{
	return isTile(x, y, z);
}

bool Level::isLightBlocker(int x, int y, int z) const
{
	return isSolidTile(x, y, z);
}

std::vector<phys::AABB> Level::getCubes(const phys::AABB& aABB) const
{
	std::vector<phys::AABB> aABBs;

	int x0 = static_cast<int>(aABB.x0);
	int y0 = static_cast<int>(aABB.y0);
	int z0 = static_cast<int>(aABB.z0);

	int x1 = static_cast<int>(aABB.x1 + 1);
	int y1 = static_cast<int>(aABB.y1 + 1);
	int z1 = static_cast<int>(aABB.z1 + 1);

	if (x0 < 0) x0 = 0;
	if (y0 < 0) y0 = 0;
	if (z0 < 0) z0 = 0;

	if (x1 > static_cast<int>(width)) x1 = static_cast<int>(width);
	if (y1 > static_cast<int>(depth)) y1 = static_cast<int>(depth);
	if (z1 > static_cast<int>(height)) z1 = static_cast<int>(height);

	for (int x = x0; x < x1; x++)
		for (int y = y0; y < y1; y++)
			for (int z = z0; z < z1; z++)
				if (isSolidTile(x, y, z))
				{
					aABBs.push_back(phys::AABB(
						static_cast<float>(x), static_cast<float>(y), static_cast<float>(z),
						static_cast<float>(x + 1), static_cast<float>(y + 1), static_cast<float>(z + 1)));
				}

	return aABBs;
}

float Level::getBrightness(int x, int y, int z) const
{
	float dark = 0.8F;
	float light = 1.0F;

	if (!(x >= 0 && y >= 0 && z >= 0 && x < static_cast<int>(width) && y < static_cast<int>(depth) && z < static_cast<int>(height))) return light;
	return (y < lightDepths[x + z * static_cast<int>(width)]) ? dark : light;
}

void Level::setTile(int x, int y, int z, uint8_t type)
{
	if (!(x >= 0 && y >= 0 && z >= 0 && x < static_cast<int>(width) && y < static_cast<int>(depth) && z < static_cast<int>(height))) return;

	blocks[(y * static_cast<int>(height) + z) * static_cast<int>(width) + x] = type;
	calcLightDepths(x, z, 1, 1);

	for (unsigned int i = 0; i < levelListeners.size(); i++)
		levelListeners[i]->tileChanged(x, y, z);
}

}  // namespace minecraft::level