#include <minecraft/level/levelRenderer.hpp>

#include <minecraft/level/frustum.hpp>
#include <minecraft/level/tile.hpp>
#include <minecraft/phys/AABB.hpp>

#include <cmath>
#include <minecraft/cpp_utils/time/time.hpp>


namespace minecraft::level
{

LevelRenderer::LevelRenderer(Level& level) : level(&level)
{
	level.addListener(this);

	xChunks = level.width / 16;
	yChunks = level.depth / 16;
	zChunks = level.height / 16;

	chunksCount = xChunks * yChunks * zChunks;
	chunks.reserve(chunksCount);

	for (uint32_t y = 0; y < yChunks; y++)
		for (uint32_t x = 0; x < xChunks; x++)
			for (uint32_t z = 0; z < zChunks; z++) {
				int x0 = static_cast<int>(x * 16);
				int y0 = static_cast<int>(y * 16);
				int z0 = static_cast<int>(z * 16);
				int x1 = static_cast<int>((x + 1) * 16);
				int y1 = static_cast<int>((y + 1) * 16);
				int z1 = static_cast<int>((z + 1) * 16);

				// На случай, если мир имеет размеры не кратные 16
				if (x1 > static_cast<int>(level.width)) x1 = static_cast<int>(level.width);
				if (y1 > static_cast<int>(level.depth)) y1 = static_cast<int>(level.depth);
				if (z1 > static_cast<int>(level.height)) z1 = static_cast<int>(level.height);

				chunks.push_back(std::move(Chunk(level, x0, y0, z0, x1, y1, z1)));
			}
}

void LevelRenderer::render([[maybe_unused]] const Player& player, uint32_t layer)
{
	Chunk::rebuiltThisFrame = 0;
	Frustum frustum = Frustum::getFrustum();

	for (uint32_t i = 0; i < chunksCount; i++)
		if (frustum.cubeInFrustum(chunks[i].aabb))
			chunks[i].render(layer);
}

void LevelRenderer::pick(const Player& player)
{
	float r = 3.0F;

	phys::AABB box = player.bb.grow(r, r, r);
	int x0 = static_cast<int>(box.x0);
	int x1 = static_cast<int>(box.x1 + 1);
	int y0 = static_cast<int>(box.y0);
	int y1 = static_cast<int>(box.y1 + 1);
	int z0 = static_cast<int>(box.z0);
	int z1 = static_cast<int>(box.z1 + 1);
	glInitNames();

	for (int x = x0; x < x1; x++) {
		glPushName(static_cast<GLuint>(x));

		for (int y = y0; y < y1; y++) {
			glPushName(static_cast<GLuint>(y));

			for (int z = z0; z < z1; z++) {
				glPushName(static_cast<GLuint>(z));

				if (level->isSolidTile(x, y, z)) {
					glPushName(0); // todo: ???

					for (int i = 0; i < 6; ++i) {
						glPushName(static_cast<GLuint>(i));
						t.init();
						Tile::rock.renderFace(t, x, y, z, i);
						t.flush();
						glPopName();
					}

					glPopName();
				}

				glPopName();
			}

			glPopName();
		}

		glPopName();
	}
}

void LevelRenderer::renderHit(const HitResult& h)
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	double time = static_cast<double>(cpp_utils::time::getTimeMicrosecond()) / 1E6;
	glColor4f(1, 1, 1, static_cast<float>(sin(time * 10)) * 0.2F + 0.4F);
	t.init();
	Tile::rock.renderFace(t, h.x, h.y, h.z, h.f);
	t.flush();
	glDisable(GL_BLEND);
}

void LevelRenderer::setDirty(int _x0, int _y0, int _z0, int _x1, int _y1, int _z1)
{
	if (_x0 < 0) _x0 = 0;
	if (_y0 < 0) _y0 = 0;
	if (_z0 < 0) _z0 = 0;

	uint32_t x0 = static_cast<uint32_t>(_x0 / 16);
	uint32_t y0 = static_cast<uint32_t>(_y0 / 16);
	uint32_t z0 = static_cast<uint32_t>(_z0 / 16);
	uint32_t x1 = static_cast<uint32_t>(_x1 / 16);
	uint32_t y1 = static_cast<uint32_t>(_y1 / 16);
	uint32_t z1 = static_cast<uint32_t>(_z1 / 16);

	
	if (x1 >= xChunks) x1 = xChunks - 1;
	if (y1 >= yChunks) y1 = yChunks - 1;
	if (z1 >= zChunks) z1 = zChunks - 1;

	for (uint32_t y = y0; y <= y1; y++)
		for (uint32_t x = x0; x <= x1; x++)
			for (uint32_t z = z0; z <= z1; z++) {
				chunks[(y * xChunks + x) * zChunks + z].setDirty();
			}
}

void LevelRenderer::tileChanged(int x, int y, int z)
{
	setDirty(x - 1, y - 1, z - 1, x + 1, y + 1, z + 1);
}

void LevelRenderer::lightColumnChanged(int x, int z, int y0, int y1)
{
	setDirty(x - 1, y0 - 1, z - 1, x + 1, y1 + 1, z + 1);
}

void LevelRenderer::allChanged()
{
	setDirty(0, 0, 0, static_cast<int>(level->width), static_cast<int>(level->depth), static_cast<int>(level->height));
}

}  // namespace minecraft::level