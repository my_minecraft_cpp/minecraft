#include <minecraft/level/tile.hpp>

#include <minecraft/cpp_utils/math/math.hpp>


using minecraft::cpp_utils::math::equal_float;

namespace minecraft::level
{

Tile Tile::rock = Tile(0);
Tile Tile::grass = Tile(1);

void Tile::render(Tesselator &t, const Level& level, uint32_t layer, int x, int y, int z)
{
	// Установка текстуры
	float u0 = static_cast<float>(tex) / 16.0F;
	float u1 = u0 + 0.0624375F; // 1 / 16 - 1 / 16000
	float v0 = 0;
	float v1 = v0 + 0.0624375F;

	// Расстановка света
	float c1 = 1.0F; // Нет блоков
	float c2 = 0.8F; // Над боковой гранью есть блок
	float c3 = 0.6F; // Над боковой гранью есть блок

	// Растановка координат плоскости куба
	float x0 = static_cast<float>(x + 0);
	float x1 = static_cast<float>(x + 1);
	float y0 = static_cast<float>(y + 0);
	float y1 = static_cast<float>(y + 1);
	float z0 = static_cast<float>(z + 0);
	float z1 = static_cast<float>(z + 1);

	float br;

	// Нижняя грань (-y)
	if (!level.isSolidTile(x, y - 1, z)) {
		br = level.getBrightness(x, y - 1, z) * c1;
		if (equal_float(br, c1) ^ (layer == 1)) {
			t.color(br, br, br);
			t.tex(u0, v1);
			t.vertex(x0, y0, z1);
			t.tex(u0, v0);
			t.vertex(x0, y0, z0);
			t.tex(u1, v0);
			t.vertex(x1, y0, z0);
			t.tex(u1, v1);
			t.vertex(x1, y0, z1);
		}
	}

	// Верхняя грань (+y)
	if (!level.isSolidTile(x, y + 1, z)) {
		br = level.getBrightness(x, y, z) * c1;
		if (equal_float(br, c1) ^ (layer == 1)) {
			t.color(br, br, br);
			t.tex(u1, v1);
			t.vertex(x1, y1, z1);
			t.tex(u1, v0);
			t.vertex(x1, y1, z0);
			t.tex(u0, v0);
			t.vertex(x0, y1, z0);
			t.tex(u0, v1);
			t.vertex(x0, y1, z1);
		}
	}

	// Боковая грань (-z)
	if (!level.isSolidTile(x, y, z - 1)) {
		br = level.getBrightness(x, y, z - 1) * c2;
		if (equal_float(br, c2) ^ (layer == 1)) {
			t.color(br, br, br);
			t.tex(u1, v0);
			t.vertex(x0, y1, z0);
			t.tex(u0, v0);
			t.vertex(x1, y1, z0);
			t.tex(u0, v1);
			t.vertex(x1, y0, z0);
			t.tex(u1, v1);
			t.vertex(x0, y0, z0);
		}
	}

	// Боковая грань (+z)
	if (!level.isSolidTile(x, y, z + 1)) {
		br = level.getBrightness(x, y, z + 1) * c2;
		if (equal_float(br, c2) ^ (layer == 1)) {
			t.color(br, br, br);
			t.tex(u0, v0);
			t.vertex(x0, y1, z1);
			t.tex(u0, v1);
			t.vertex(x0, y0, z1);
			t.tex(u1, v1);
			t.vertex(x1, y0, z1);
			t.tex(u1, v0);
			t.vertex(x1, y1, z1);
		}
	}

	// Боковая грань (-x)
	if (!level.isSolidTile(x - 1, y, z)) {
		br = level.getBrightness(x - 1, y, z) * c3;
		if (equal_float(br, c3) ^ (layer == 1)) {
			t.color(br, br, br);
			t.tex(u1, v0);
			t.vertex(x0, y1, z1);
			t.tex(u0, v0);
			t.vertex(x0, y1, z0);
			t.tex(u0, v1);
			t.vertex(x0, y0, z0);
			t.tex(u1, v1);
			t.vertex(x0, y0, z1);
		}
	}

	// Боковая грань (+x)
	if (!level.isSolidTile(x + 1, y, z)) {
		br = level.getBrightness(x + 1, y, z) * c3;
		if (equal_float(br, c3) ^ (layer == 1)) {
			t.color(br, br, br);
			t.tex(u0, v1);
			t.vertex(x1, y0, z1);
			t.tex(u1, v1);
			t.vertex(x1, y0, z0);
			t.tex(u1, v0);
			t.vertex(x1, y1, z0);
			t.tex(u0, v0);
			t.vertex(x1, y1, z1);
		}
	}
}

void Tile::renderFace(Tesselator &t, int x, int y, int z, int face)
{
	float x0 = static_cast<float>(x + 0);
	float x1 = static_cast<float>(x + 1);
	float y0 = static_cast<float>(y + 0);
	float y1 = static_cast<float>(y + 1);
	float z0 = static_cast<float>(z + 0);
	float z1 = static_cast<float>(z + 1);

	if (face == 0)
	{
		t.vertex(x0, y0, z1);
		t.vertex(x0, y0, z0);
		t.vertex(x1, y0, z0);
		t.vertex(x1, y0, z1);
	}

	if (face == 1)
	{
		t.vertex(x1, y1, z1);
		t.vertex(x1, y1, z0);
		t.vertex(x0, y1, z0);
		t.vertex(x0, y1, z1);
	}

	if (face == 2)
	{
		t.vertex(x0, y1, z0);
		t.vertex(x1, y1, z0);
		t.vertex(x1, y0, z0);
		t.vertex(x0, y0, z0);
	}

	if (face == 3)
	{
		t.vertex(x0, y1, z1);
		t.vertex(x0, y0, z1);
		t.vertex(x1, y0, z1);
		t.vertex(x1, y1, z1);
	}

	if (face == 4)
	{
		t.vertex(x0, y1, z1);
		t.vertex(x0, y1, z0);
		t.vertex(x0, y0, z0);
		t.vertex(x0, y0, z1);
	}

	if (face == 5)
	{
		t.vertex(x1, y0, z1);
		t.vertex(x1, y0, z0);
		t.vertex(x1, y1, z0);
		t.vertex(x1, y1, z1);
	}
}

}  // namespace minecraft::level