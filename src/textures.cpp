#include <minecraft/textures.hpp>

#include <cmrc/cmrc.hpp>
#ifdef _WIN32
	#include <Windows.h>
#endif
#include <GL/glu.h>

#include <minecraft/cpp_utils/image/image.hpp>


CMRC_DECLARE(minecraft::rc);

using minecraft::cpp_utils::image::Image;


namespace minecraft
{

std::map<std::string, GLuint> Textures::idMap = std::map<std::string, GLuint>();

GLuint Textures::loadTexture(const std::string& resourceName, int mode)
{
	std::map<std::string, GLuint>::iterator it = Textures::idMap.find(resourceName);
	if (it != Textures::idMap.end()) return it->second;

	GLuint newTexID;
	glGenTextures(1, &newTexID);

	Textures::idMap.insert({ resourceName, newTexID });
	printf("%s -> %d\n", resourceName.c_str(), newTexID);

	glBindTexture(GL_TEXTURE_2D, newTexID);

	// Установка параметров отоброжения текстуры
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, mode);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, mode);

	// Создание и загрузка png изображения
	cmrc::embedded_filesystem fs = cmrc::minecraft::rc::get_filesystem();
	cmrc::file texture_fd = fs.open(resourceName);
	Image texture = Image::load_png_image(static_cast<const void*>(&(*texture_fd.begin())), texture_fd.size());

	// Загрузка готовой текстуры в OpenGL
	if (texture)
	{
		if (texture.channels == 3)
			gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB, static_cast<GLsizei>(texture.width), static_cast<GLsizei>(texture.height), GL_RGB, GL_UNSIGNED_BYTE, texture.data.data());
		else if (texture.channels == 4)
			gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA, static_cast<GLsizei>(texture.width), static_cast<GLsizei>(texture.height), GL_RGBA, GL_UNSIGNED_BYTE, texture.data.data());
		else
			return 0;
		return newTexID;
	}

	return 0;
}

}  // namespace minecraft