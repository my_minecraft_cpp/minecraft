#include <minecraft/timer.hpp>

#include <minecraft/cpp_utils/time/time.hpp>


namespace minecraft
{

Timer::Timer(const float ticksPerSecond) : ticksPerSecond(ticksPerSecond)
{
	lastTime = cpp_utils::time::getTimeMicrosecond();
}

void Timer::advanceTime()
{
	uint64_t now = cpp_utils::time::getTimeMicrosecond();
	uint64_t passedUs = now - lastTime;
	lastTime = now;

	if (passedUs > MAX_US_PER_UPDATE) passedUs = MAX_US_PER_UPDATE;

	fps = static_cast<float>(US_PER_SECOND) / static_cast<float>(passedUs);
	passedTime += static_cast<float>(passedUs) * timeScale * ticksPerSecond / static_cast<float>(US_PER_SECOND); // Расчет прошедшего времени (сек.)

	ticks = static_cast<int>(passedTime); // Расчет прошедших тиков
	if (ticks > MAX_TICKS_PER_UPDATE) ticks = MAX_TICKS_PER_UPDATE;

	passedTime -= static_cast<float>(ticks);
	a = passedTime;
}

}  // namespace minecraft