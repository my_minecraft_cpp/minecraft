#include <minecraft/rubyDung.hpp>

#include <cstring>
#include <cmrc/cmrc.hpp>
#ifdef _WIN32
	#include <Windows.h>
#endif
#include <GL/glu.h>

#include <minecraft/cpp_utils/time/time.hpp>
#include <minecraft/cpp_utils/input/mouse.hpp>
#include <minecraft/cpp_utils/input/keyboard.hpp>
#include <minecraft/cpp_utils/image/image.hpp>


CMRC_DECLARE(minecraft::rc);

using minecraft::cpp_utils::input::MouseButtonEvent;
using minecraft::cpp_utils::input::Mouse;
using minecraft::cpp_utils::input::Keyboard;
using minecraft::cpp_utils::image::Image;


namespace minecraft
{

void framebuffer_size_callback([[maybe_unused]] GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
}

int RubyDung::init()
{
	srand(static_cast<uint32_t>(cpp_utils::time::getTimeMillisecond()));

	// Initialize the library
    if (!glfwInit()) return -1;

	// Create a windowed mode window and its OpenGL context
	GLFWmonitor* monitor = glfwGetPrimaryMonitor();
	const GLFWvidmode* mode = glfwGetVideoMode(monitor);
	glfwWindowHint(GLFW_RED_BITS, mode->redBits);
	glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
	glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
	glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);

	width = mode->width; height = mode->height;
    window = glfwCreateWindow(width, height, "RubyDung", monitor, NULL);
    if (!window) return -1;
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

	// Load window icon
	{
		cmrc::embedded_filesystem fs = cmrc::minecraft::rc::get_filesystem();
		cmrc::file window_icon_fd = fs.open("objects/icon.png");
		Image window_icon = Image::load_png_image(static_cast<const void*>(&(*window_icon_fd.begin())), window_icon_fd.size());
		GLFWimage glfw_window_icon {static_cast<int>(window_icon.width), static_cast<int>(window_icon.height), window_icon.data.data()};
		glfwSetWindowIcon(window, 1, &glfw_window_icon);
	}

	// Init keyboard and mouse
	Mouse::init(window);
	Keyboard::init(window);
	Mouse& mouse = Mouse::getInstance();
	mouse.showCursor(false);
	mouse.setCursorPos(width / 2, height / 2);
	mouse.registerButtonCallback();

	// Make the window's context current
    glfwMakeContextCurrent(window);


	glEnable(GL_TEXTURE_2D);

	glShadeModel(GL_SMOOTH);
	glClearColor(0.5F, 0.8F, 1, 0);
	glClearDepth(1);
	glEnable(GL_DEPTH_TEST);

	glEnable(GL_FOG);
	glFogi(GL_FOG_MODE, GL_EXP);
	glFogf(GL_FOG_DENSITY, 0.2F);
	glFogfv(GL_FOG_COLOR, fogColor);
	glDisable(GL_FOG);

	glEnable(GL_CULL_FACE); // Рисование только передних граней

	glDepthFunc(GL_LEQUAL);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);

	level = new level::Level(256, 256, 64);
	levelRenderer = new level::LevelRenderer(*level);
	player = Player(*level);

	for (int i = 0; i < 100; i++)
		zombies.push_back(std::move(character::Zombie(*level, 128, 0, 128)));

	return 0;
}

void RubyDung::destroy()
{
	level->save();

	glfwTerminate();
	if (hitResult) delete hitResult;
	if (levelRenderer) delete levelRenderer;
	if (level) delete level;	
}

void RubyDung::moveCameraToPlayer(float a)
{
	// Поворот головы
	glTranslatef(0, 0, -0.3F); // вращать относительно шеи
	glRotatef(player.xRot, 1, 0, 0);
	glRotatef(player.yRot, 0, 1, 0);

	// Установка позиции игрока
	float x = player.xo + (player.x - player.xo) * a;
	float y = player.yo + (player.y - player.yo) * a;
	float z = player.zo + (player.z - player.zo) * a;

	glTranslatef(-x, -y, -z);
}

void RubyDung::setupCamera(float a)
{
	// Установка параметров проекционной матрици (преспективы)
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(70, static_cast<float>(width) / static_cast<float>(height), 0.05F, 1000);

	// Установка параметров модально-видовой матрици (положение в пространстве)
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	moveCameraToPlayer(a);
}

void RubyDung::setupPickCamera(float a, int x, int y)
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	
	memset(viewportBuffer, 0, 16 * sizeof(GLint));
	glGetIntegerv(GL_VIEWPORT, viewportBuffer);

	gluPickMatrix(x, y, 5, 5, viewportBuffer); // Задать область сканирования
	gluPerspective(70, static_cast<float>(width) / static_cast<float>(height), 0.05F, 1000);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	moveCameraToPlayer(a);
}

void RubyDung::pick(float a)
{
	memset(selectBuffer, 0, 2000 * sizeof(GLuint));
	glSelectBuffer(2000, selectBuffer);

	glRenderMode(GL_SELECT); // Включение режима выбора

	setupPickCamera(a, width / 2, height / 2); // Установка камеры

	levelRenderer->pick(player); // Установить мнимые полигоны

	int hits = glRenderMode(GL_RENDER); // Получить количество мнимых полигонов под курсором
	int closest = 0; // Минимальное из минимального по Z (ближайший обьект)
	int names[10];
	GLuint hitNameCount = 0;
	
	GLuint lastGet = 0;

	for (int i = 0; i < hits; i++)
	{
		const GLuint& nameCount = selectBuffer[lastGet]; // Количество элементов использущих описание одной мнимой плоскости (всегда 5)
		
		lastGet += 1;
		const int minZ = static_cast<int>(selectBuffer[lastGet]); // Минимальное по Z (измеряется в попугаях)
		
		lastGet += 2; // Перемотка на идентификаторы (пропускаем максимальное по Z)

		// Если обьект ниже (в попугаях) closest и объект не первый
		if (minZ >= closest && i != 0)
		{
			// Пропускаем обьект
			lastGet += nameCount;
		}
		else
		{
			closest = minZ;
			hitNameCount = nameCount;

			// Сохраняем идентификаторы объекта в массив names
			for (uint32_t j = 0; j < nameCount; j++)
			{
				names[j] = static_cast<int>(selectBuffer[lastGet]);
				lastGet += 1;
			}
		}
	}

	// Если есть хоть один "выделенный" объект
	if (hitNameCount > 0)
	{
		if (hitResult != NULL) delete hitResult;
		hitResult = new HitResult(names[0], names[1], names[2], names[3], names[4]);
	}
	else 
	{
		if (hitResult != NULL) delete hitResult;
		hitResult = NULL;
	}
}

void RubyDung::run()
{
	try
	{
		if (init() != 0)
		{
			printf("Failed to start RubyDung\n");
			destroy();
			return;
		}
	}
	catch(...)
	{
		printf("Uncatched exception while starting RubyDung\n");
	}

	Keyboard& keyboard = Keyboard::getInstance();
	uint64_t lastTime = cpp_utils::time::getTimeMillisecond();
	int frames = 0;

	// Loop until the user closes the window
    while (!glfwWindowShouldClose(window) && !keyboard.isKeyDown(GLFW_KEY_ESCAPE))
    {
		timer.advanceTime();
		for (int e = 0; e < timer.ticks; e++) tick();

		render(timer.a);
		glfwSwapBuffers(window);  // Swap front and back buffers
		frames++;

		while (cpp_utils::time::getTimeMillisecond() >= lastTime + 1000) {
			printf("%d fps, %d\n", frames, level::Chunk::updates);
			level::Chunk::updates = 0;
			lastTime += 1000;
			frames = 0;
		}

        /* Poll for and process events */
        glfwPollEvents();
    }

	destroy();
}

void RubyDung::tick()
{
	for (character::Zombie& z : zombies) z.tick();

	player.tick();
}

void RubyDung::render(float a)
{
	int xpos, ypos;
	Mouse& mouse = Mouse::getInstance();
	mouse.getCursorPos(&xpos, &ypos);
	mouse.setCursorPos(width / 2, height / 2);

	// // Поворот игрока, учитывая изменения положения мыши
	player.turn(static_cast<float>(xpos - width / 2), static_cast<float>(height / 2 - ypos));

	pick(a);

	while (mouse.nextButtonEvent())
	{
		MouseButtonEvent& e = mouse.getButtonEvent();

		if (e.button == GLFW_MOUSE_BUTTON_LEFT && e.action == GLFW_PRESS && hitResult != NULL)
			level->setTile(hitResult->x, hitResult->y, hitResult->z, false);

		if (e.button == GLFW_MOUSE_BUTTON_RIGHT && e.action == GLFW_PRESS && hitResult != NULL)
		{
			int x = hitResult->x;
			int y = hitResult->y;
			int z = hitResult->z;

			if (hitResult->f == 0) y--;
			if (hitResult->f == 1) y++;
			if (hitResult->f == 2) z--;
			if (hitResult->f == 3) z++;
			if (hitResult->f == 4) x--;
			if (hitResult->f == 5) x++;

			level->setTile(x, y, z, true);
		}
	}


	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	setupCamera(a);

	levelRenderer->render(player, 0);

	for (character::Zombie& z : zombies) z.render(a);

	glEnable(GL_FOG);
	levelRenderer->render(player, 1);  // Пещеры
	glDisable(GL_TEXTURE_2D);
	if (hitResult != NULL) levelRenderer->renderHit(*hitResult);

	glDisable(GL_FOG);
}

}  // namespace minecraft