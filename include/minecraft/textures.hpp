#pragma once

#include <string>
#include <map>
#include <GLFW/glfw3.h>


namespace minecraft
{

class Textures
{
private:
	static std::map<std::string, GLuint> idMap;

public:
	Textures() {};

	static GLuint loadTexture(const std::string& resourceName, int mode);
};

}  // namespace minecraft