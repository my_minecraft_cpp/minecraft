#pragma once

#include <minecraft/level/level.hpp>
#include <minecraft/level/tesselator.hpp>


namespace minecraft::level
{

/*
 * Класс Tile позволяет рисовать блок с натянутой текстурой и распредилением света
 */
class Tile
{
private:
	int tex;

public:
	static Tile rock;
	static Tile grass;

	Tile(int tex) : tex(tex) {};

	void render(Tesselator &t, const Level& level, uint32_t layer, int x, int y, int z); // Рисовать с текстурой и светом
	void renderFace(Tesselator &t, int x, int y, int z, int face); // Рисовать отдельно плоскость (face)
};

}  // namespace minecraft::level