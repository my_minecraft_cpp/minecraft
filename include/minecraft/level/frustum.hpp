#pragma once

#include <GLFW/glfw3.h>

#include <minecraft/phys/AABB.hpp>


namespace minecraft::level
{

class Frustum
{
public:
	static const int RIGHT = 0, LEFT = 1, BOTTOM = 2, TOP = 3, BACK = 4, FRONT = 5;
	static const int A = 0, B = 1, C = 2, D = 3;

private:
	static Frustum frustum;

	GLfloat proj[16];
	GLfloat modl[16];
	GLfloat clip[16];

	void normalizePlane(float(*frustum)[4], int side);
	void calculateFrustum();

public:
	float m_Frustum[6][4];

	static Frustum& getFrustum()
	{
		frustum.calculateFrustum();
		return frustum;
	}

	bool pointInFrustum(const float x, const float y, const float z) const;
	bool sphereInFrustum(const float x, const float y, const float z, const float radius) const;
	bool cubeFullyInFrustum(const float x1, const float y1, const float z1, const float x2, const float y2, const float z2) const;
	bool cubeInFrustum(const float x1, const float y1, const float z1, const float x2, const float y2, const float z2) const;
	bool cubeInFrustum(const phys::AABB& aabb) const;
};

}  // namespace minecraft::level
