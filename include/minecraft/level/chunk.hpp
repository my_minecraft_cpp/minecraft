#pragma once

#include <minecraft/level/level.hpp>
#include <minecraft/level/tesselator.hpp>
#include <minecraft/level/tile.hpp>
#include <minecraft/textures.hpp>
#include <minecraft/phys/AABB.hpp>


namespace minecraft::level
{

/*
 * Класс Chunk позволяет работать с чанками
 */
class Chunk
{
private:
	bool dirty = true; // true - обновить чанк перед рендерингом
	GLuint lists = 0; // Переменная для сохранения индекса листа сцены
	static Tesselator t;

	void rebuild(uint32_t layer); // Обновить чанк

public:
	Level* level; // Карта мира
	phys::AABB aabb; // Место, которое занимает чанк
	int x0, y0, z0; // Координаты нижнего угла
	int x1, y1, z1; // Координаты верхнего угла

	static int rebuiltThisFrame;
	static int updates;

	Chunk() {};
	Chunk(Level& level, int x0, int y0, int z0, int x1, int y1, int z1);

	void render(uint32_t layer); // Рендер чанка
	void setDirty() { dirty = true; } // Пометить чанк для обновления
};

}  // namespace minecraft::level