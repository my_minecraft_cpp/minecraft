#pragma once

namespace minecraft::level
{

class LevelListener
{
public:
	virtual ~LevelListener() {};

	virtual void tileChanged([[maybe_unused]] int x, [[maybe_unused]] int y, [[maybe_unused]] int z) {};
	virtual void lightColumnChanged([[maybe_unused]] int x, [[maybe_unused]] int z, [[maybe_unused]] int y0, [[maybe_unused]] int y1) {};
	virtual void allChanged() {};
};

}  // namespace minecraft::level