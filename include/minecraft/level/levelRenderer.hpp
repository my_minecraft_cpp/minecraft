#pragma once

#include <vector>

#include <minecraft/level/chunk.hpp>
#include <minecraft/level/level.hpp>
#include <minecraft/level/levelListener.hpp>
#include <minecraft/level/tesselator.hpp>
#include <minecraft/hitResult.hpp>
#include <minecraft/player.hpp>


namespace minecraft::level
{

/*
 * Класс LevelRenderer используется для рендеринга мира и пометки чанков для обновления
 */
class LevelRenderer : public LevelListener
{
private:
	static const int CHUNK_SIZE = 16;

	Level* level; // Карта мира
	std::vector<Chunk> chunks; // Массив чанков
	uint32_t chunksCount; // Количество чанков

	uint32_t xChunks, yChunks, zChunks; // количество чанков по осям

	Tesselator t;

public:
	LevelRenderer() : LevelListener() {};
	LevelRenderer(Level& level);

	void render(const Player& player, uint32_t layer); // Рендер мира

	void pick(const Player& player); // Создать псевдо-визуализацию для определения, на какой блок смотрит игрок
	void renderHit(const HitResult& h); // Рендеринг плитки HitResult

	void setDirty(int x0, int y0, int z0, int x1, int y1, int z1); // Пометить регион (x0, y0, z0, x1, y1, z1) для обновления
	void tileChanged(int x, int y, int z); // Пометить один блок для обновления
	void lightColumnChanged(int x, int z, int y0, int y1); // Пометить чанки для обновления, которые задеваются столбцом x, z, ограниченным снизу y0, сверху y1
	void allChanged(); // Пометить весь мир для обновления
};

}  // namespace minecraft::level