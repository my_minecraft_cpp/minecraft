#pragma once

#include <cstdint>
#include <vector>

#include <minecraft/level/levelListener.hpp>
#include <minecraft/phys/AABB.hpp>


namespace minecraft::level
{

/*
 * Класс Level используется для хранения и обработки мира
 */
class Level
{
private:
	uint8_t* blocks = NULL;  // Массив, в котором хранятся индексы блоков, структура: (y * height + z) * width + x
	int* lightDepths;  // Массив, в котором хранится самый высокий уровень (y) в кахдом стольбце, на котором был найден блок, структура: z * width + x
	std::vector<LevelListener*> levelListeners;

public:
	uint32_t width, height, depth;  // Размер мира

	Level() {};
	Level(uint32_t w, uint32_t h, uint32_t d);
	~Level();

	void load();
	void save() const;
	void calcLightDepths(int x0, int y0, int x1, int y1);

	void addListener(LevelListener* levelListener);
	void removeListener(LevelListener* levelListener);

	bool isTile(int x, int y, int z) const;
	bool isSolidTile(int x, int y, int z) const;
	bool isLightBlocker(int x, int y, int z) const;

	std::vector<phys::AABB> getCubes(const phys::AABB& aABB) const;
	float getBrightness(int x, int y, int z) const;
	void setTile(int x, int y, int z, uint8_t type);
};

}  // namespace minecraft::level