#pragma once

#include <GLFW/glfw3.h>


namespace minecraft::level
{

class Tesselator
{
private:
	static const int MAX_VERTICES = 10000;
	float vertexBuffer[MAX_VERTICES * 3];
	float texCoordBuffer[MAX_VERTICES * 2];
	float colorBuffer[MAX_VERTICES * 3];
	int vertices;

	float u, v;
	float r, g, b;

	bool hasColor, hasTexture;

	void clear()
	{
		vertices = 0;
	}

public:
	Tesselator() { init(); }

	void init()
	{
		clear();
		hasColor = false;
		hasTexture = false;
	}

	void tex(float u, float v)
	{
		hasTexture = true;
		this->u = u;
		this->v = v;
	}

	void color(float r, float g, float b)
	{
		hasColor = true;
		this->r = r;
		this->g = g;
		this->b = b;
	}

	void vertex(float x, float y, float z);

	void flush();
};

}  // namespace minecraft::level