#pragma once

namespace minecraft::phys
{

/*
 * Класс AABB позволяет работать с регионами.
 */
class AABB
{
private:
	float epsilon = 0; // Зазор между столкновениями регионов

public:
	float x0, y0, z0; //нижняя точка региона
	float x1, y1, z1; //верхняя точка региона
	//Всегда верны неравенства: x0 < x1   y0 < y1   z0 < z1

	AABB() {}

	/*
	 * Для создания региона нужно указать его крайние точки
	 */
	AABB(float x0, float y0, float z0, float x1, float y1, float z1) :
		x0(x0), y0(y0), z0(z0), x1(x1), y1(y1), z1(z1) {}

	/*
	 * Метод expand расширяет регион по алгаритму: если входное значение положительное,
	 * то регион разширяется вверх, если отрицательное - вниз
	 */
	AABB expand(float xa, float ya, float za) const;

	/*
	 * Метод grow расширяет или сжимает регион по всем осям в зависимости от входных значений
	 */
	AABB grow(float xa, float ya, float za) const;

	/*
	 * Метод clip*Collide возвращает максимальное значение сдвига текущего региона
	 * до столкновения с регионом c (по соответствующей оси) с зазором epsilon, но
	 * не больше входного значения по абсолютной величине
	 */
	float clipXCollide(const AABB& c, float xa) const;
	float clipYCollide(const AABB& c, float ya) const;
	float clipZCollide(const AABB& c, float za) const;

	/*
	 * Метод intersects возвращает true, если данный регион и регион c пересекаются.
	 */
	bool intersects(const AABB& c) const;

	/*
	 * Метод move сдвигает данный регион по всем осям
	 */
	void move(float xa, float ya, float za);
};

}  // namespace minecraft::phys
