#pragma once

#include <cstdint>


namespace minecraft
{

/*
 *  Класс Timer позволяет работать со временем: расчитывать fps, количество
 *  прошедших тиков и др.
 */
class Timer
{
private:
	const static uint64_t US_PER_SECOND = 1000000; // Количество микросекунд в секунде
	const static uint64_t MAX_US_PER_UPDATE = 1000000; // Max. задержка (микросекунд) между вызовами advanceTime
	const static int MAX_TICKS_PER_UPDATE = 100; // Max. кол-во прошедших тиков между вызовами advanceTime

	float ticksPerSecond; // Кол-во тиков в секунду
	uint64_t lastTime;

public:
	int ticks; // Целая часть количества прошедших тиков между вызовами advanceTime
	float a; // Дробная часть количества прошедших тиков между вызовами advanceTime
	float timeScale = 1; // Масштаб времени (1 - стандарт)
	float fps = 0; // Текущее количество кадров в секунду
	float passedTime = 0;

	Timer() {};
	Timer(const float ticksPerSecond);

	void advanceTime();
};

}  // namespace minecraft

