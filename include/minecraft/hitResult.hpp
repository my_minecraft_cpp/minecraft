#pragma once


namespace minecraft
{

/*
 * Класс HitResult позволяет структурировать набор переменных, отвечающих
 * за блок, на который смотрит игрок.
 */
class HitResult
{
public:
	int x, y, z; // Координаты блока
	int o; 		 // В данной версии это не используется
	int f; 		 // Номер стороны, на которую смотрит игрок

	HitResult() {};
	HitResult(int x, int y, int z, int o, int f) : x(x), y(y), z(z), o(o), f(f) {};
};

}
