#pragma once

#include <vector>
#include <GLFW/glfw3.h>

#include <minecraft/hitResult.hpp>
#include <minecraft/player.hpp>
#include <minecraft/timer.hpp>
#include <minecraft/character/zombie.hpp>
#include <minecraft/level/level.hpp>
#include <minecraft/level/levelRenderer.hpp>


namespace minecraft
{

class RubyDung
{
private:
	int width;
	int height;
	float fogColor[4] = { 14.0F / 255, 11.0F / 255, 10.0F / 255, 1 };
	
	GLFWwindow* window;
	Timer timer = Timer(60.0F);
	level::Level* level = NULL;
	level::LevelRenderer* levelRenderer = NULL;

	Player player;
	std::vector<character::Zombie> zombies;
	GLint viewportBuffer[16];
	GLuint selectBuffer[2000];

	HitResult* hitResult = NULL;

	void moveCameraToPlayer(float a);
	void setupCamera(float a);
	void setupPickCamera(float a, int x, int y);
	void pick(float a);

public:
	int init();
	void destroy();

	void run();
	void tick();

	void render(float a);
};

}  // namespace minecraft