#pragma once

#include <minecraft/level/level.hpp>
#include <minecraft/phys/AABB.hpp>


namespace minecraft
{

class Entity
{
private:
	level::Level* level;

	void setPos(float x, float y, float z);

protected:
	float heightOffset = 0; // Высота глаз (считая от ног)

	void resetPos();

public:
	float x, y, z; // Позиция игрока
	float xo, yo, zo; // Предыдущая позиция игрока
	float xd = 0, yd = 0, zd = 0; // Ускорение движения игрока

	float yRot = 0, xRot = 0; // Направление обзора

	phys::AABB bb; // Регион, который "занимает" игрок
	bool onGround = false; // Игрок стоит на земле

	Entity() {};
	Entity(level::Level& level) : level(&level) { resetPos(); }
	virtual ~Entity() {};

	void turn(float xo, float yo);
	virtual void tick();
	void move(float xa, float ya, float za);
	void moveRelative(float xa, float za, float speed);
};

}