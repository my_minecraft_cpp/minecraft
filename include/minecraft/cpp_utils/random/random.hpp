#pragma once

namespace minecraft::cpp_utils::random
{

float uniform_random();

}  // namespace minecraft::cpp_utils::random