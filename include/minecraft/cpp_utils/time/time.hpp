#pragma once

#include <cstdint>


namespace minecraft::cpp_utils::time
{

uint64_t getTimeNanosecond();
uint64_t getTimeMicrosecond();
uint64_t getTimeMillisecond();

}  // namespace minecraft::cpp_utils::time