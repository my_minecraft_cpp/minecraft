#pragma once

namespace minecraft::cpp_utils::math
{

bool equal_float(float a, float b);

}  // namespace minecraft::cpp_utils::math