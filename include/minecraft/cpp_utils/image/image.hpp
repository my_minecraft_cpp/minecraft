#pragma once

#include <vector>
#include <cstdlib>
#include <cstdint>


namespace minecraft::cpp_utils::image
{

class Image
{
private:
	Image(std::vector<uint8_t>& data, uint32_t height, uint32_t width, uint32_t depth, uint32_t channels, bool transparency)
		: data(data), height(height), width(width), depth(depth), channels(channels), transparency(transparency) {}

public:
	std::vector<uint8_t> data;
	uint32_t height = 0, width = 0;
	uint32_t depth = 0;
	uint32_t channels = 0;
	bool transparency = false;

	Image() {}

	static bool is_png_image(const void* data, size_t size);
	static Image load_png_image(const void* data, size_t size);

	operator bool() const { return !data.empty(); }
};

}  // namespace minecraft::cpp_utils::image