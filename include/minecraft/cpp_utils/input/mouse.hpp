#pragma once

#include <queue>
#include <GLFW/glfw3.h>


namespace minecraft::cpp_utils::input
{

struct MouseButtonEvent
{
	int button;
	int action;
	int mods;
};

class Mouse
{
private:
	std::queue<MouseButtonEvent> queueButtonEvent;
	MouseButtonEvent lastButtonEvent;
    GLFWwindow* window;
    static Mouse instance;

	static void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);

public:
    static void init(GLFWwindow* window) { instance.window = window; };
    static Mouse& getInstance() { return instance; }

	void registerButtonCallback();  // Зарегистрировать внутренний button callback

	bool nextButtonEvent();
	MouseButtonEvent& getButtonEvent() { return lastButtonEvent; };

	void setInputMode(int mode, int value) const;
	void showCursor(bool show) const;
	void setCursorPos(int xpos, int ypos) const;

    void getCursorPos(int* xpos, int* ypos) const;
	bool isButtonDown(int button) const;
};

}  // namespace minecraft::cpp_utils::input