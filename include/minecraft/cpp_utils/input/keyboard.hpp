#pragma once

#include <GLFW/glfw3.h>


namespace minecraft::cpp_utils::input
{

class Keyboard
{
private:
    GLFWwindow* window;
    static Keyboard instance;

public:
    static void init(GLFWwindow* window) { instance.window = window; };
    static Keyboard& getInstance() { return instance; }

    bool isKeyDown(int key) const;
};

}  // namespace minecraft::cpp_utils::input