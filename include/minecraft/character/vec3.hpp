#pragma once

namespace minecraft::character
{

/*
 * Класс Vec3 позволяет работать с координатами точки в 3d
 */
class Vec3
{
public:
	float x, y, z;

	Vec3() {};
	Vec3(float x, float y, float z) : x(x), y(y), z(z) {};

	void set(float x, float y, float z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}

	/*
	 * Найти промежуочную точку между текущей и точкой t
	 */
	Vec3 interpolateTo(const Vec3& t, float p) const;
};

}  // namespace minecraft::character
