#pragma once

#include <vector>
#include <GLFW/glfw3.h>

#include <minecraft/character/vertex.hpp>


namespace minecraft::character
{

class Polygon
{
public:
	std::vector<Vertex> vertices;

	Polygon() {};
	Polygon(const std::vector<Vertex>& vertices) : vertices(vertices) {};
	Polygon(const std::vector<Vertex>& vertices, int u0, int v0, int u1, int v1) :
		vertices({
			vertices[0].remap(static_cast<float>(u1), static_cast<float>(v0)),
			vertices[1].remap(static_cast<float>(u0), static_cast<float>(v0)),
			vertices[2].remap(static_cast<float>(u0), static_cast<float>(v1)),
			vertices[3].remap(static_cast<float>(u1), static_cast<float>(v1))
		}) {};

	void render() const;
};

}  // namespace minecraft::character
