#pragma once

#include <minecraft/character/vec3.hpp>


namespace minecraft::character
{

/*
 * Класс Vertex позволяет сопоставить вершину с текстурой
 */
class Vertex
{
public:
	Vec3 pos; // Координаты вершины
	float u, v; // Координаты текстуры

	Vertex() {};
	Vertex(float x, float y, float z, float u, float v) : pos(x, y, z), u(u), v(v) {};
	Vertex(const Vertex& vertex, float u, float v) : pos(vertex.pos), u(u), v(v) {};
	Vertex(Vec3 pos, float u, float v) : pos(pos), u(u), v(v) {};

	Vertex remap(float u, float v) const
	{
		return Vertex(pos, u, v);
	};
};

}  // namespace minecraft::character
