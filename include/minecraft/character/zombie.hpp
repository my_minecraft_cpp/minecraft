#pragma once

#include <minecraft/character/cube.hpp>
#include <minecraft/entity.hpp>
#include <minecraft/textures.hpp>
#include <minecraft/level/level.hpp>


namespace minecraft::character
{

class Zombie : public Entity
{
public:
	Cube head;
	Cube body;
	Cube arm0, arm1;
	Cube leg0, leg1;

	float rot;
	float timeOffs;
	float speed;
	float rotA;

	Zombie() {};
	Zombie(level::Level& level, float x, float y, float z);

	void tick();
	void render(float a);
};

}  // namespace minecraft::character