#pragma once

#include <vector>
#include <GLFW/glfw3.h>

#include <minecraft/character/polygon.hpp>
#include <minecraft/character/vertex.hpp>


namespace minecraft::character
{

/*
 * Класс Cube групперует полигоны в параелипипиды с текстурами
 */
class Cube
{
private:
	std::vector<Vertex> vertices;
	std::vector<Polygon> polygons;

	int xTexOffs = 0;
	int yTexOffs = 0;

public:
	float x = 0, y = 0, z = 0;
	float xRot = 0, yRot = 0, zRot = 0;

	Cube() {};
	Cube(const int xTexOffs, const int yTexOffs) : xTexOffs(xTexOffs), yTexOffs(yTexOffs) {};

	void setTexOffs(const int xTexOffs, const int yTexOffs)
	{
		this->xTexOffs = xTexOffs;
		this->yTexOffs = yTexOffs;
	};

	void setPos(const float x, const float y, const float z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}

	void addBox(const float x0, const float y0, const float z0, const int w, const int h, const int d);
	void render() const;
};

}  // namespace minecraft::character