#pragma once

#include <minecraft/entity.hpp>
#include <minecraft/level/level.hpp>


namespace minecraft
{

class Player : public Entity
{
private:

public:
	Player() {};
	Player(level::Level& level);

	void tick();
};

}  // namespace minecraft
