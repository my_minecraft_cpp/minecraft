# MyMinecraft C++ (rd-132328)

Cross-platform C++ implementation of [pre-classic rd-132328](https://minecraft.fandom.com/ru/wiki/Pre-classic_rd-132328).

![](.gitlab/game_logo.png)

## Download

See [releases](https://gitlab.com/my_minecraft_cpp/minecraft/-/releases/rd-132328)